const pkg = require("./package");
const nodeExternals = require("webpack-node-externals");
const webpack = require("webpack");
module.exports = {
  //mode: "universal",

  head: {
    title: "Dazlure",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: pkg.description }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
      }
    ]
  },

  loading: { color: "#FFFFFF" },

  css: [
    "~/assets/css/sec.scss",
    "~/node_modules/froala-editor/css/froala_editor.pkgd.min.css",
    "~/node_modules/froala-editor/css/froala_style.min.css"
  ],

  modules: ["@nuxtjs/pwa", "@nuxtjs/workbox", "@nuxtjs/axios"],

  axios: {
    baseURL: "https://dazlure.com"
  },
  manifest: {
    name: "Dazlure",
    description:
      "Are you looking to provide beauty treatments to clients on a part-time basis? Join Dazlure beauty platform and earn up to £50/hour for just one treatment!",
    theme_color: "#000"
  },
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
      })
    ],
    postcss: {
      plugins: {
        "postcss-custom-properties": false,
        autoprefixer: {}
      }
    },
    /*
    ** You can extend webpack config here
    */
    extend(config, { isServer }) {
      // ...
      if (isServer) {
        config.externals = [
          nodeExternals({
            // default value for `whitelist` is
            // [/es6-promise|\.(?!(?:js|json)$).{1,5}$/i]
            whitelist: [
              /es6-promise|\.(?!(?:js|json)$).{1,5}$/i,
              /^vue-awesome/
            ]
          })
        ];
      }
    }
  },
  plugins: [
    { src: "~/plugins/modals.js", ssr: false },
    { src: "~/plugins/eventBus.js", ssr: false },
    { src: "~/plugins/vue-awesome.js", ssr: false },
    { src: "~/plugins/vue-datetime.js", ssr: false },
    { src: "~/plugins/vue-moment.js", ssr: false },
    { src: "~/plugins/froala.js", ssr: false }
  ]
};
