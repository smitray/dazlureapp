export const state = () => ({
  blocks: []
});
export const getters = {
  getBlocks: state => {
    return state.blocks;
  }
};
export const mutations = {
  addBlocks: (state, blocks) => {
    state.blocks = blocks;
  },
  addSingleBlock: (state, block) => {
    state.blocks.push(block);
  },
  editBlock: (state, block) => {
    state.blocks = state.blocks.filter(el => el._id !== block._id);
    state.blocks.push(block);
  },
  deleteBlock: (state, block) => {
    state.blocks = state.blocks.filter(el => el._id !== block._id);
  }
};
