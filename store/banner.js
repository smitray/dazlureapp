export const state = () => ({
  banners: []
});
export const getters = {
  getBanners: state => {
    return state.banners;
  }
};
export const mutations = {
  addBanners: (state, banners) => {
    state.banners = banners;
  },
  addSingleBanner: (state, banner) => {
    state.banners.push(banner);
  },
  editBanner: (state, banner) => {
    state.banners = state.banners.filter(el => el._id !== banner._id);
    state.banners.push(banner);
  },
  deleteBanner: (state, banner) => {
    state.banners = state.banners.filter(el => el._id !== banner._id);
  }
};
