export const state = () => ({
  categories: [],
  services: []
});
export const getters = {
  getCategories: state => {
    return state.categories;
  },
  getServices: state => {
    return state.services;
  }
};
export const mutations = {
  addCategories: (state, categories) => {
    state.categories = categories;
  },
  addServices: (state, services) => {
    state.services = services;
  },
  addSingleCategory: (state, category) => {
    state.categories.push(category);
  },
  editCategory: (state, category) => {
    state.categories = state.categories.filter(el => el._id !== category._id);
    state.categories.push(category);
  },
  addSingleService: (state, service) => {
    state.services.push(service);
  },
  deleteService: (state, service) => {
    state.services = state.services.filter(el => el._id !== service._id);
  },
  deleteCategory: (state, category) => {
    state.categories = state.categories.filter(el => el._id !== category._id);
  },
  editService: (state, service) => {
    state.services = state.services.filter(el => el._id !== service._id);
    state.services.push(service);
  }
};
