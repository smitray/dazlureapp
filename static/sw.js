importScripts('/_nuxt/workbox.3de3418b.js')

const workboxSW = new self.WorkboxSW({
  "cacheId": "dazlure-app",
  "clientsClaim": true,
  "directoryIndex": "/"
})

workboxSW.precache([
  {
    "url": "/_nuxt/app.cb5c1c665ad4b2cc981d.js",
    "revision": "0b80f30713d7fde0a60296f60b17ca34"
  },
  {
    "url": "/_nuxt/layouts_dashboardLayout.8ec732e729171f58a6e2.js",
    "revision": "5d945c74247a7762cb63e0d40e927f2f"
  },
  {
    "url": "/_nuxt/layouts_default.a9c8b131616c83630c83.js",
    "revision": "4366150db9ceb24923e14db69923ee01"
  },
  {
    "url": "/_nuxt/layouts_loginLayout.146b744a34c5265933e4.js",
    "revision": "c029620dfd44c3b05211e646ac8a3b52"
  },
  {
    "url": "/_nuxt/manifest.a42b4ee5f033d197b9fe.js",
    "revision": "c014de6175be081aa31ba9ecb0c7238d"
  },
  {
    "url": "/_nuxt/pages_dashboard_applicant.de88fd673861a6c4d753.js",
    "revision": "11ce6672eadaaf86d5e3b630f0e60f5e"
  },
  {
    "url": "/_nuxt/pages_dashboard_customer.23780aded11302e12304.js",
    "revision": "342f4eea3b883586f837aba5742c1208"
  },
  {
    "url": "/_nuxt/pages_dashboard_frontsite.35d14eae9cf16c1e6417.js",
    "revision": "788d3f86da127e694388d9407cde5741"
  },
  {
    "url": "/_nuxt/pages_dashboard_index.8da62801854a541b7c0d.js",
    "revision": "3c180845e856b9fb07dd1f9eb07bf641"
  },
  {
    "url": "/_nuxt/pages_dashboard_professional.78a915a59781d98b7c84.js",
    "revision": "ce9e3bf5c0b92b30cdcfc336d2d2032e"
  },
  {
    "url": "/_nuxt/pages_dashboard_service.a9b361ddbf485c1ecbfd.js",
    "revision": "a0db8f925f4c37b9fe296c7d9252e904"
  },
  {
    "url": "/_nuxt/pages_index.209df7fc47c01e3794d0.js",
    "revision": "5ffddf004f3043035cc333fb168c0801"
  },
  {
    "url": "/_nuxt/vendor.c3949e8717005ee4fb63.js",
    "revision": "0368c359c728af83302cd908454a95e6"
  },
  {
    "url": "/_nuxt/workbox.3de3418b.js",
    "revision": "a9890beda9e5f17e4c68f42324217941"
  }
])


workboxSW.router.registerRoute(new RegExp('/_nuxt/.*'), workboxSW.strategies.cacheFirst({}), 'GET')

workboxSW.router.registerRoute(new RegExp('/.*'), workboxSW.strategies.networkFirst({}), 'GET')

